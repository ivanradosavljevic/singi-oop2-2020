/**
 * Proširit klasu radnik metodom bool izvrsiNalog(nalog).
 * Ova metoda prima argument tipa nalog i ukoliko je status
 * naloga false postavlja ga na true vraća true, u suprotnom vraća false.
 * Ova metoda je javno dostupna.
 */

#include <iostream>
#include <string>

class Nalog
{
private:
    static int brojacNaloga;

protected:
    int brojNaloga;
    bool status;

public:
    Nalog(bool status = false) : status(status),
                                 brojNaloga(brojacNaloga)
    {
        brojacNaloga++;
    }

    bool getStatus()
    {
        return status;
    }
    void setStatus(bool status)
    {
        this->status = status;
    }
};

int Nalog::brojacNaloga = 1;

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Nalog nalog;
    Radnik r("Ime", "Prezime", 10000, 0.1);
    if (!nalog.getStatus())
    {
        ispravniKoraci++;
    }
    if (r.izvrsiNalog(nalog))
    {
        ispravniKoraci++;
    }
    if (nalog.getStatus())
    {
        ispravniKoraci++;
    }
    if (!r.izvrsiNalog(nalog))
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 4)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}