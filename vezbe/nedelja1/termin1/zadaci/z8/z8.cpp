/**
 * Napisati klasu Dokument koja ima protected atribute naslov i autor.
 * Dodati podrazumevani konstruktor, konstruktor sa parametrima i get i set metode.
 * Dodati metodu stampaj() koja ispisuje autora i naslov dokumenta.
 * 
 * Napisati klasu Nalog koja nasleđuje klasu Dokument i uvodi privatni
 * statički atribut brojacNaloga i protected atribute brojNaloga tipa int i status tipa bool.
 * Broj naloga inicijalno postaviti na vrednost 1.
 * Dodati podrazumevani konstruktor, konstruktor sa parametrima i get i set metode.
 * Prilikom poziva ovih konstruktora uvećati vrednost atributa brojacNaloga,
 * a vrednost atributa brojNaloga postaviti na trenutnu vrednost atributa brojacNaloga.
 */

#include <iostream>
#include <string>

int Nalog::brojacNaloga = 1;

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Dokument dokument("Naslov", "Autor");
    Nalog nalog1("Naslov1", "Autor1");
    Nalog nalog2("Naslov2", "Autor2", true);
    if (dokument.getNaslov() == "Naslov" &&
        dokument.getAutor() == "Autor")
    {
        ispravniKoraci++;
    }
    dokument.setNaslov("n");
    dokument.setAutor("a");
    if (dokument.getNaslov() == "n" &&
        dokument.getAutor() == "a")
    {
        ispravniKoraci++;
    }
    if (nalog1.getBrojNaloga() == 1)
    {
        ispravniKoraci++;
    }
    if (nalog2.getBrojNaloga() == 2)
    {
        ispravniKoraci++;
    }
    if (nalog2.getStatus() == true)
    {
        ispravniKoraci++;
    }
    nalog2.setStatus(false);
    if (nalog2.getStatus() == false)
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 6)
    {
        dokument.stampaj();
        std::cout << std::endl;
        nalog1.stampaj();
        std::cout << std::endl;
        nalog2.stampaj();
        std::cout << std::endl;
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}