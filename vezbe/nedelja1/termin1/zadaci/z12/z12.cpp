/**
 * Proširit klasu Radnik atributom radnoMesto koji je referenca na objekat tipa Preduzece.
 * Ovaj atribut je privatan i može mu se pristupati samo preko get i set metoda.
 * Proširiti metodu detalji u klasi Radnik tako da ispisuje i radno mesto radnika.
 */

#include <iostream>
#include <string>

class Preduzece
{
private:
    std::string naziv;
    std::string adresa;

public:
    Preduzece() {}
    Preduzece(std::string naziv, std::string adresa)
    {
        this->naziv = naziv;
        this->adresa = adresa;
    }

    void setNaziv(std::string naziv)
    {
        this->naziv = naziv;
    }

    void detalji()
    {
        std::cout << naziv << " " << adresa;
    }
};

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata << " ";
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Preduzece preduzece1("Test preduzece1", "Test adresa1");
    Preduzece preduzece2("Test preduzece2", "Test adresa2");
    Radnik r("Ime", "Prezime", 10000, 0.1, preduzece1);
    if (&r.getRadnoMesto() == &preduzece1)
    {
        ispravniKoraci++;
    }
    r.setRadnoMesto(preduzece2);
    if (&r.getRadnoMesto() == &preduzece1)
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 2)
    {
        r.detalji();
        std::cout << std::endl << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}