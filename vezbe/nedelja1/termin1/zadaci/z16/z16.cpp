/**
 * Napisati klasu Dokuemnt koja sadrži atribute naslov, sadržaj i tip.
 * Dodati konstruktore, get i set metode i metodu stampaj koja prikazuje vrednosti atributa.
 * Napisati klasu SlozeniDokument koja nasleđuje klasu Dokument i sadrži referencu na drugi dokument.
 * Redefinisati metodu stampaj tako da prilikom ispisa podatka prvo ispiše podatke o složenom
 * dokumentu a potom o dokumentu koji je smešten u njemu. Ispis treba da se vrši proizvoljno
 * u dubinu.
 */

#include <iostream>
#include <string>

int main()
{
    //Test ispravnosti resenja.
    //Instancirati jedan dokument i dva slozena dokumenta.
    //Uvezati ih sve jedan za drugim.
    //Pozvati metodu stampaj nad poslednjim dokumentom.

    //Kraj testa.
    return 0;
}