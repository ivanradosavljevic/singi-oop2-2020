/**
 * Upotrebom debugger-a prnaći i otkloniti sve greške u programu.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }
    double getOsnovicaOsiguranja()
    {
        return osnovicaOsiguranja;
    }
    void setOsnovicaOsiguranja(double osnovicaOsiguranja)
    {
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }
    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata << std::endl;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

int main()
{
    Radnik marko("Marko", "Petrovic", 0, 0.15);
    marko.detalji();
    std::cout << "Bruto plata: " << marko.brutoPlata() << std::endl;

    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Radnik testRadnik;
    testRadnik.setIme("Ime");
    testRadnik.setPrezime("Prezime");
    testRadnik.setPlata(1000);
    testRadnik.setOsnovicaOsiguranja(0.1);

    if (testRadnik.getIme() == "Ime")
    {
        ispravniKoraci++;
    }
    if (testRadnik.getPrezime() == "Prezime")
    {
        ispravniKoraci++;
    }
    if (testRadnik.getPlata() == 1000)
    {
        ispravniKoraci++;
    }
    if (testRadnik.getOsnovicaOsiguranja() == 0.1)
    {
        ispravniKoraci++;
    }

    if (ispravniKoraci == 4 && (std::abs(testRadnik.brutoPlata() - (1000 + 1000 * 0.1)) < 0.1e-10))
    {
        testRadnik.detalji();
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}