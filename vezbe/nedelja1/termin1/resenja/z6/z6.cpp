/**
 * Napisati klasu Vozac koja nasleđuje klasu Radnik.
 * Klasa vozač ima dodatni privatni atribut, kategorija, koji je tipa string.
 * Napisati konstruktor za klasu Vozač i redefinisati metodu detalji.
 * Konstruktor i metoda detalji su javno vidljivi.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata, double osnovicaOsiguranja)
    {
        this->ime = ime;
        this->prezime = prezime;
        this->plata = plata;
        this->osnovicaOsiguranja = osnovicaOsiguranja;
    }

    std::string getIme()
    {
        return ime;
    }
    void setIme(std::string ime)
    {
        this->ime = ime;
    }
    std::string getPrezime()
    {
        return prezime;
    }
    void setPrezime(std::string prezime)
    {
        this->prezime = prezime;
    }
    double getPlata()
    {
        return plata;
    }
    void setPlata(double plata)
    {
        this->plata = plata;
    }

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << ", visina plate: " << plata;
    }

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

class Vozac : public Radnik
{
private:
    std::string kategorija;

public:
    Vozac(std::string ime, std::string prezime, double plata,
          double osnovicaOsiguranja, std::string kategorija) : Radnik(ime, prezime, plata, osnovicaOsiguranja),
                                                               kategorija(kategorija)
    {
    }
    void detalji()
    {
        Radnik::detalji();
        std::cout << ", ima dozvolu za kategoriju " << kategorija;
    }
};

int main()
{
    //Test ispravnosti resenja.
    Vozac testVozac("Jakov", "Djuric", 55000, 0.2, "C");

    if (std::abs(testVozac.brutoPlata() - (55000 + 55000 * 0.2)) < 0.1e-10)
    {
        testVozac.detalji();
        std::cout << std::endl;
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.

    return 0;
}