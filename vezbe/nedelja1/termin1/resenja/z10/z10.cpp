/**
 * Napisati klasu Proizvod sa protected atributima naziv, opis i cena.
 * Dodati podrazumevani konstruktor i konstruktor sa parametrima.
 * Dodati get i set metode i metodu detalji koja ispisuje vrednost atributa.
 * Konstruktori i metode su javno vidvljivi.
 * 
 * Napisati klasu prehrambeni proizvod koja nasleđuje klasu proizvod.
 * Ova klasa uvodi privatni atribut rok trajanja tipa int koji predstavlja
 * broj meseci do isteka roka trajanja.
 * Pored podred podrazumevanog konstruktora, konstruktora sa parametrima i get i set metoda
 * uvesti metodu bool ispravnost() koja vraća true ukoliko je vrednost atributa rok trajanja
 * veća od 0, a u suprotnom vraća false.
 * Redefinisati metodu detalji tako da se za PrehrambeniProizvod dodatno ispisuje rok trajanja
 * i ispravnost proizvoda.
 */

#include <iostream>
#include <string>

class Proizvod
{
protected:
    std::string naziv;
    std::string opis;
    double cena;

public:
    Proizvod() {}
    Proizvod(std::string naziv, std::string opis, double cena) : naziv(naziv), opis(opis), cena(cena) {}
    std::string getNaziv()
    {
        return naziv;
    }
    void setNaziv(std::string naziv)
    {
        this->naziv = naziv;
    }
    std::string getOpis()
    {
        return opis;
    }
    void setOpis(std::string opis)
    {
        this->opis = opis;
    }
    double getCena()
    {
        return cena;
    }
    void setCena(double cena)
    {
        this->cena = cena;
    }
    void detalji()
    {
        std::cout << naziv << " " << opis << ", " << cena;
    }
};

class PrehrambeniProizvod : public Proizvod
{
private:
    int rokTrajanja;

public:
    PrehrambeniProizvod() : Proizvod() {}
    PrehrambeniProizvod(std::string naziv, std::string opis,
                        double cena, int rokTrajanja) : Proizvod(naziv, opis, cena),
                                                        rokTrajanja(rokTrajanja) {}
    bool ispravnost()
    {
        if (rokTrajanja > 0)
        {
            return true;
        }
        return false;
    }
    void detalji()
    {
        Proizvod::detalji();
        std::cout << "; rok trajanja: " << rokTrajanja;
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Proizvod proizvod("Naziv", "Opis", 100);
    PrehrambeniProizvod prehrambeniProizvod1("Naziv", "Opis", 100, 1);
    PrehrambeniProizvod prehrambeniProizvod2("Naziv", "Opis", 100, 0);
    if (proizvod.getNaziv() == "Naziv" &&
        proizvod.getOpis() == "Opis" &&
        proizvod.getCena() == 100)
    {
        ispravniKoraci++;
    }
    proizvod.setNaziv("n");
    proizvod.setOpis("o");
    proizvod.setCena(10);

    if (proizvod.getNaziv() == "n" &&
        proizvod.getOpis() == "o" &&
        proizvod.getCena() == 10)
    {
        ispravniKoraci++;
    }
    if (prehrambeniProizvod1.ispravnost())
    {
        ispravniKoraci++;
    }
    if (!prehrambeniProizvod2.ispravnost())
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 4)
    {
        prehrambeniProizvod1.detalji();
        std::cout << std::endl;
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}