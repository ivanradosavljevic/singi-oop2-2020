/**
 * Napisati klasu poruka sa atributima naslov, sadržaj, posiljalac i primalac.
 * Atributi posiljalac i primalac su reference na objekte tipa Radnik.
 * U klasu poruka dodati metodu stampaj koja vrši ispis vrednosti atributa kao i get i set metode.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime) : ime(ime), prezime(prezime) {}

    void detalji()
    {
        std::cout << "Radnik: " << ime << " " << prezime << std::endl;
    }
};

class Poruka
{
private:
    std::string naslov;
    std::string sadrzaj;
    Radnik &posiljalac;
    Radnik &primalac;

public:
    Poruka(std::string naslov, std::string sadrzaj, Radnik &posiljalac, Radnik &primalac) : naslov(naslov),
                                                                                            sadrzaj(sadrzaj),
                                                                                            posiljalac(posiljalac),
                                                                                            primalac(primalac) {}
    std::string getNaslov()
    {
        return naslov;
    }
    void setNaslov(std::string naslov)
    {
        this->naslov = naslov;
    }
    std::string getSadrzaj()
    {
        return sadrzaj;
    }
    void setSadrzaj(std::string sadrzaj)
    {
        this->sadrzaj = sadrzaj;
    }
    Radnik &getPosiljalac()
    {
        return posiljalac;
    }
    void setPosiljalac(Radnik &posiljalac)
    {
        this->posiljalac = posiljalac;
    }
    Radnik &getPrimalac()
    {
        return primalac;
    }
    void setPrimalac(Radnik &primalac)
    {
        this->primalac = primalac;
    }
    void stampaj()
    {
        std::cout << naslov << " " << sadrzaj << std::endl;
        posiljalac.detalji();
        primalac.detalji();
    }
};

int main()
{
    //Test ispravnosti resenja.
    int ispravniKoraci = 0;
    Radnik posiljalac1("Posiljalac1", "Posiljlac1");
    Radnik posiljalac2("Posiljalac2", "Posiljlac2");
    Radnik primalac1("Primalac1", "Primalac1");
    Radnik primalac2("Primalac2", "Primalac2");
    Poruka poruka("Naslov", "Sadrzaj", posiljalac1, primalac1);

    if (&poruka.getPosiljalac() == &posiljalac1)
    {
        ispravniKoraci++;
    }
    poruka.setPosiljalac(posiljalac2);
    if (&poruka.getPosiljalac() == &posiljalac1)
    {
        ispravniKoraci++;
    }
    if (&poruka.getPrimalac() == &primalac1)
    {
        ispravniKoraci++;
    }
    poruka.setPrimalac(primalac2);
    if (&poruka.getPrimalac() == &primalac1)
    {
        ispravniKoraci++;
    }
    if (ispravniKoraci == 4)
    {
        poruka.stampaj();
        std::cout << std::endl
                  << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}