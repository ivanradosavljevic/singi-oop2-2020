/**
 * Napisati klasu Dokuemnt koja sadrži atribute naslov, sadržaj i tip.
 * Dodati konstruktore, get i set metode i metodu stampaj koja prikazuje vrednosti atributa.
 * Napisati klasu SlozeniDokument koja nasleđuje klasu Dokument i sadrži referencu na drugi dokument.
 * Redefinisati metodu stampaj tako da prilikom ispisa podatka prvo ispiše podatke o složenom
 * dokumentu a potom o dokumentu koji je smešten u njemu. Ispis treba da se vrši proizvoljno
 * u dubinu.
 */

#include <iostream>
#include <string>

class Dokument
{
private:
    std::string naslov;
    std::string sadrzaj;
    std::string tip;

public:
    Dokument() {}
    Dokument(std::string naslov, std::string sadrzaj, std::string tip = "dokument") : naslov(naslov), sadrzaj(sadrzaj), tip(tip) {}
    std::string getNaslov()
    {
        return naslov;
    }
    void setNaslov(std::string naslov)
    {
        this->naslov = naslov;
    }
    std::string getSadrzaj()
    {
        return sadrzaj;
    }
    void setSadrzaj(std::string sadrzaj)
    {
        this->sadrzaj = sadrzaj;
    }
    std::string getTip()
    {
        return tip;
    }
    void setTip(std::string tip)
    {
        this->tip = tip;
    }
    void stampaj()
    {
        std::cout << naslov << " " << sadrzaj << " " << tip << std::endl;
    }
};

class SlozeniDokument : public Dokument
{
private:
    Dokument &uvezaniDokument;

public:
    SlozeniDokument(std::string naslov, std::string sadrzaj,
                    Dokument &uvezaniDokument) : Dokument(naslov, sadrzaj, "slozeni"),
                                                 uvezaniDokument(uvezaniDokument) {}
    void stampaj()
    {
        Dokument::stampaj();
        if (uvezaniDokument.getTip() == "slozeni")
        {
            SlozeniDokument &dokument = (SlozeniDokument &)uvezaniDokument;
            dokument.stampaj();
        }
        else
        {
            uvezaniDokument.stampaj();
        }
    }
};

int main()
{
    //Test ispravnosti resenja.
    //Instancirati jedan dokument i dva slozena dokumenta.
    //Uvezati ih sve jedan za drugim.
    //Pozvati metodu stampaj nad poslednjim dokumentom.
    Dokument d1("Naslov", "Sadrzaj");
    SlozeniDokument d2("Naslov2", "Sadrzaj2", d1);
    SlozeniDokument d3("Naslov3", "Sadrzaj3", d2);
    d3.stampaj();
    //Kraj testa.
    return 0;
}