#include "Automobil.hpp"

Automobil::Automobil() : Vozilo()
{
}
Automobil::Automobil(string proizvodjac,
                     string model,
                     string tipKaroserije) : Vozilo(proizvodjac, model),
                                             tipKaroserije(tipKaroserije)
{
}

void Automobil::ispis(ostream &output) const
{
    Vozilo::ispis(output);
    output << tipKaroserije << endl;
}
void Automobil::ucitaj(istream &input)
{
    Vozilo::ucitaj(input);
    input >> tipKaroserije;
}
string Automobil::getTipKaroserije() const
{
    return tipKaroserije;
}
Automobil::~Automobil()
{
}