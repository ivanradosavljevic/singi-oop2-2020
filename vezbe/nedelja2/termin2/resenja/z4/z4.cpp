/**
 * Proširiti prethodni zadatak uvođenjem sledećih funkcionalnosti:
 * Spajanje dva niza operatorom +.
 * Omogućiti ispis kolekcije na izlazni tok operatorom <<.
 * Omogućiti učitavanje kolekcije sa ulaznog toka operatorom >>.
 * Prepraviti zadatak tako de se može učitati i zapisati bilo
 * koji tip vozila. Omogućiti prilikom učitavanja podataka
 * o voznom parku da nazivi proizvođača, modela i tipa
 * karoserije sadrže razmake.
 */

#include <iostream>
#include <fstream>
#include <vector>

#include "Vozilo.hpp"
#include "Automobil.hpp"
#include "Niz.hpp"

using namespace std;

typedef Niz<Vozilo *> VozniPark;

ostream &operator<<(ostream &output, const Vozilo *vozilo)
{
    vozilo->ispis(output);
    return output;
}

istream &operator>>(istream &input, Vozilo *vozilo)
{
    vozilo->ucitaj(input);
    return input;
}

ostream &operator<<(ostream &output, const VozniPark &vozniPark)
{
    output << vozniPark.velicina() << endl;
    for (int i = 0; i < vozniPark.velicina(); i++)
    {
        output << vozniPark[i] << endl;
    }
    return output;
}

istream &operator>>(istream &input, VozniPark &vozniPark)
{
    int duzina;
    input >> duzina;
    for (int i = 0; i < duzina; i++)
    {
        Vozilo *v = new Automobil();
        input >> v;
        vozniPark.dodaj(v);
    }
    return input;
}

int main()
{
    VozniPark vp1;
    VozniPark vp2;
    vp1.dodaj(new Automobil("Proizvodjac1", "Model1", "Karoserija1"));
    vp1.dodaj(new Automobil("Proizvodjac2", "Model2", "Karoserija2"));
    vp1.dodaj(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp1.dodaj(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));

    vp2.dodaj(new Automobil("Proizvodjac5", "Model5", "Karoserija5"));
    vp2.dodaj(new Automobil("Proizvodjac6", "Model6", "Karoserija6"));

    VozniPark vp3 = vp1 + vp2;

    ofstream output("vozila.txt");
    output << vp3;
    output.close();

    VozniPark vp4;
    ifstream input("vozila.txt");
    input >> vp4;
    input.close();

    cout << vp4 << endl;
    
    return 0;
}