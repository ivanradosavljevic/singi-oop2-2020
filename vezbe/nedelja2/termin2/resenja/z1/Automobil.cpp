#include "Automobil.hpp"

Automobil::Automobil() : Vozilo()
{
}
Automobil::Automobil(string proizvodjac,
                     string model,
                     string tipKaroserije) : Vozilo(proizvodjac, model),
                                             tipKaroserije(tipKaroserije)
{
}
void Automobil::detalji()
{
    Vozilo::detalji();
    cout << tipKaroserije << endl;
}
Automobil::~Automobil()
{
}