#ifndef KOLEKCIJA_HPP
#define KOLEKCIJA_HPP

template<typename T> class Kolekcija
{
public:
    virtual int velicina() const = 0;
    virtual void dodaj(T vrednost) = 0;
    virtual void ukloni(int indeks) = 0;
    virtual T dobavi(int indeks) const = 0;
    virtual ~Kolekcija() = 0;
};

template<typename T>
Kolekcija<T>::~Kolekcija()
{
}

#endif