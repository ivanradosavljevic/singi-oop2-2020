#ifndef VOZILO_HPP
#define VOZILO_HPP

#include <iostream>
#include <string>

using namespace std;
class Vozilo
{
private:
    string proizvodjac;
    string model;

public:
    Vozilo();
    Vozilo(string proizvodjac, string model);
    string getProizvodjac() const;
    void setProizvodjac(string proizvodjac);
    string getModel() const;
    void setModel(string model);
    virtual void detalji() const = 0;
    virtual ~Vozilo();
};

#endif