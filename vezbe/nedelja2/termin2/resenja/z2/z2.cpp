/**
 * Proširiti prethodni zadatak tako da se uvedu ograničenja
 * za pristup, brisanje i dodavanje podataka.
 * Optimizovati kolekciju tako da se proširivanje niza
 * vrši samo po potrebi, odnosno uvesti promenljivu kapacitet
 * koja definiše veličinu niza i promenljivu brojElemenata
 * koja definiše popunjenost niza. Kada brojElemenata dostigne
 * kapacitet kopirati postojeći niz u novi niz duplo većeg
 * kapaciteta. Prepraviti uklanjanje elemenata tako da se
 * elementi pomeraju u levo bez prethodnog kreiranja novog niza.
 */

#include <iostream>

#include "Vozilo.hpp"
#include "Automobil.hpp"
#include "VozniPark.hpp"

using namespace std;

int main()
{
    VozniPark *vp = new VozniPark();
    vp->dodajVozilo(new Automobil("Proizvodjac1", "Model1", "Karoserija1"));
    vp->dodajVozilo(new Automobil("Proizvodjac2", "Model2", "Karoserija2"));
    vp->dodajVozilo(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp->dodajVozilo(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));

    vp->ukloniVozilo(0);

    vp->dodajVozilo(new Automobil("Proizvodjac1", "Model1", "Karoserija1"));
    vp->dodajVozilo(new Automobil("Proizvodjac2", "Model2", "Karoserija2"));
    vp->dodajVozilo(new Automobil("Proizvodjac3", "Model3", "Karoserija3"));
    vp->dodajVozilo(new Automobil("Proizvodjac4", "Model4", "Karoserija4"));

    vp->ukloniVozilo(5);
    vp->ukloniVozilo(-2);
    vp->detalji();
    delete vp;
    return 0;
}