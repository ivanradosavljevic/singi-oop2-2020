#include "Dokument.hpp"

Dokument::Dokument()
{
}
Dokument::Dokument(std::string naslov, std::string autor) : naslov(naslov), autor(autor)
{
}
std::string Dokument::getNaslov()
{
    return naslov;
}
void Dokument::stampaj()
{
    std::cout << naslov << " " << autor << std::endl;
}