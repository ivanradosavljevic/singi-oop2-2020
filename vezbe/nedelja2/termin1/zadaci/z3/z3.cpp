/**
 * Dinamički instancirati jednog radnika i jednog vozača.
 * Ispraviti greške tako da su zadovoljeni uslovi testa.
 * Premestiti deklaracije kalsa u odvojena zaglavalja a
 * implementacije u odvojene implementacione datoteke.
 * 
 * Razdvojiti klase u odvojene datoteke.
 */

#include <iostream>
#include <string>

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;
    double osnovicaOsiguranja;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime,
           double plata, double osnovicaOsiguranja) : ime(ime), prezime(prezime),
                                                      plata(plata), osnovicaOsiguranja(osnovicaOsiguranja) {}

    double brutoPlata()
    {
        return plata + (plata * osnovicaOsiguranja);
    }
};

class Vozac : public Radnik
{
private:
    std::string kategorija;

public:
    Vozac() : Radnik() {}
    Vozac(std::string ime, std::string prezime,
          double plata, double osnovicaOsiguranja, std::string kategorija) : Radnik(ime, prezime,
                                                                                    plata, osnovicaOsiguranja),
                                                                             kategorija(kategorija) {}
    double brutoPlata()
    {
        double brutoPlata = Radnik::brutoPlata();
        if (kategorija == "C")
        {
            return brutoPlata + 10000;
        }
        return brutoPlata;
    }
};

int main()
{
    //Test ispravnosti resenja.
    if (std::abs(radnik1->brutoPlata() - (10000 + 10000 * 0.1)) < 0.1e-10 &&
        std::abs(vozac1->brutoPlata() - (10000 + 10000 * 0.1 + 10000)) < 0.1e-10)
    {
        std::cout << "Zadatak je ispravno resen." << std::endl;
    }
    //Kraj testa.
    return 0;
}