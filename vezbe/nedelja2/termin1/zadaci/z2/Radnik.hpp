#ifndef RADNIK_HPP
#define RADNIK_HPP

#include <iostream>
#include <string>

using namespace std;

class Radnik
{
private:
    std::string ime;
    std::string prezime;
    double plata;

public:
    Radnik() {}
    Radnik(std::string ime, std::string prezime, double plata) : ime(ime), prezime(prezime),
                                                                 plata(plata) {}
    void detalji()
    {
        std::cout << ime << " " << prezime << " " << plata << std::endl;
    }
};
#endif