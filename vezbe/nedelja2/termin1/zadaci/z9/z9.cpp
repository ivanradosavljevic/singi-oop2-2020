/**
 * Proširiti klasu Arhiva metodom pronadjiDokumente(naslov) koja pronalazi sve dokumente
 * čiji naslov sadrži zadatu reč i vreća vektor pronađenih dokumenata.
 */

#include <iostream>

#include "Dokument.hpp"

int main()
{
    Dokument d1("Dokument 1", "test");
    Dokument d2("dokument 2", "test2");
    Dokument d3("dokument 3", "test 3");
    Arhiva a;
    a.dodajDokument(d1);
    a.dodajDokument(d2);
    a.dodajDokument(d3);
    std::vector<Dokument> dokumenti = a.pronadjiDokumente("dokument");
    for(Dokument d : dokumenti) {
        d.stampaj();
    }

    return 0;
}