/**
 * Napisati klasu Nalog koja sadrži atribute nalogodavac i sadržaj.
 * Ovi atributi su tipa string.
 * Za ovu klasu npisati konstruktore, get i set metode i metodu
 * stampaj() koja ispisuje vrednosti svih atributa.
 * 
 * Dodati klasu RadniNalog koja naseđuje nalog i dodaje atribut angazovaniRadnici.
 * Ovaj atribut je tipa string.
 * Defnisati konstruktore, get i set metode i redefinisati metodu stampaj() tako
 * da ispod osnovnih podataka naloga ispiše i podatak o angažovanim radnicima.
 * 
 * Klase napisati u odvojenim datotekama i smestiti ih u odvojen prostor imena.
 */

#include <iostream>
#include <string>

#include "Nalog.hpp"
#include "RadniNalog.hpp"

using namespace nalog;

int main()
{
    //Test ispravnosti resenja.
    Nalog nalog = Nalog("Nalog", "Sadrzaj");
    RadniNalog radniNalog = RadniNalog("Radni nalog", "Sadrzaj radnog naloga", "Radnici");
    Nalog  &n = nalog;
    Nalog &rn = radniNalog;
    std::cout << "Ispis sadrzaja naloga: " << std::endl;
    n.stampaj();
    std::cout << std::endl << "Ispis sadrzaja radnog naloga:" << std::endl;
    rn.stampaj();
    //Kraj testa.
    return 0;
}