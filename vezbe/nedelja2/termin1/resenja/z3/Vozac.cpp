#include "Vozac.hpp"

Vozac::Vozac() : Radnik() {}
Vozac::Vozac(std::string ime, std::string prezime,
             double plata, double osnovicaOsiguranja, std::string kategorija) : Radnik(ime, prezime,
                                                                                       plata, osnovicaOsiguranja),
                                                                                kategorija(kategorija) {}
double Vozac::brutoPlata()
{
    double brutoPlata = Radnik::brutoPlata();
    if (kategorija == "C")
    {
        return brutoPlata + 10000;
    }
    return brutoPlata;
}