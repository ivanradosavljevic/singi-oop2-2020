#include "Direktor.hpp"

Direktor::Direktor() : Radnik()
{
}

Direktor::Direktor(string ime, string prezime, double plata,
                   string preduzece) : Radnik(ime, prezime, plata), preduzece(preduzece)
{
}

void Direktor::detalji()
{
    Radnik::detalji();
    cout << preduzece << endl;
}