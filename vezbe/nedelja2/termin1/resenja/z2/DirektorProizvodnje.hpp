#ifndef DIREKTOR_PROIZVODNJE_HPP
#define DIREKTOR_PROIZVODNJE_HPP

#include <iostream>
#include <string>

#include "Direktor.hpp"

using namespace std;

class DirektorProizvodnje : public Direktor
{
private:
    string pogon;

protected:
public:
    DirektorProizvodnje();
    DirektorProizvodnje(string ime, string prezime, double plata, string preduzece, string pogon);
    virtual void detalji();
};

#endif