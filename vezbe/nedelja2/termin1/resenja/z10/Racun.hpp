#ifndef RACUN_HPP
#define RACUN_HPP

#include <vector>

#include "Transakcija.hpp"
#include "Uplata.hpp"
#include "Isplata.hpp"

using namespace std;

class Racun
{
private:
    double stanje;
    vector<Transakcija*> tranasakcije;

public:
    Racun();
    Racun(double stanje);
    void izvrsiUplatu(Uplata *uplata);
    void izvrsiIsplatu(Isplata *isplata);
    void izvestaj();
};

#endif