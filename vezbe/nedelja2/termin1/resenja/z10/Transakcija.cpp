#include "Transakcija.hpp"

Transakcija::Transakcija()
{
}

Transakcija::Transakcija(string svrhaUplate,
                         string pozivNaBroj, double iznos) : svrhaUplate(svrhaUplate),
                                                             pozivNaBroj(pozivNaBroj),
                                                             iznos(iznos)
{
}

string Transakcija::getSvrhaUplate()
{
    return svrhaUplate;
}
void Transakcija::setSvrhaUplate(const string &svrhaUplate)
{
    this->svrhaUplate = svrhaUplate;
}
string Transakcija::getPozivNaBroj()
{
    return pozivNaBroj;
}
void Transakcija::setPozivNaBroj(const string &pozivNaBroj)
{
    this->pozivNaBroj = pozivNaBroj;
}
double Transakcija::getIznos()
{
    return iznos;
}
void Transakcija::setIznos(double iznos)
{
    this->iznos = iznos;
}

void Transakcija::detalji()
{
    cout << "Transakcija " << svrhaUplate << " " << pozivNaBroj << " " << iznos << endl;
}