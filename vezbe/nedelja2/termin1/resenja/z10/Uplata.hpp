#ifndef UPLATA_HPP
#define UPLATA_HPP

#include "Transakcija.hpp"

class Uplata : public Transakcija
{
protected:
    string uplatilac;

public:
    Uplata();
    Uplata(string svrhaUplate, string pozivNaBroj, string uplatilac, double iznos);
    string getUplatilac();
    void setUplatilac(string uplatilac);
    void detalji();
};

#endif