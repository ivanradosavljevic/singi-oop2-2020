/**
 * Napisati klasu Transakcija koja ima atribute svrha uplate, poziv na broj i iznos.
 * Ova klasa ima podrazumevani konstruktor i konstruktor sa parametrima.
 * 
 * Napisati klasu Uplata koja nasleđuje klasu Transakcija i dodaje atribut uplatilac.
 * Napisati klasu Isplata koja nasleđuje klasu Transakcija i dodaje atribut primalac.
 * Obe klase imaju podrazumevani i konstruktor sa parametrima kao i get i set metode.
 * 
 * Napisati klasu račun koja ima atribute stanje i transakciije.
 * Atribut transakcije je vektor uplata i isplata.
 * Ova klasa ima podrazumevani konstruktor i konstruktor sa parametrima.
 * Dodatno ova klasa uvodi metode izvrsiUplatu(uplata) i izvrsiIsplatu(isplata).
 * Ove metode primljene argumente smeštaju u vektor transakcija i uvećavaju
 * odnosno umanjuju stanje na računu. Dodati metodu za izvestaj() koja ispisuje
 * detalje svih transakcija i na kraju ukupno stanje na računu.
 * Konstruktori i sve metode ove klase su javno dostupni.
 */

#include <iostream>
#include <string>
#include <vector>

#include "Transakcija.hpp"
#include "Uplata.hpp"
#include "Isplata.hpp"
#include "Racun.hpp"

int main() {
    Uplata *u1 = new Uplata("a1", "b1", "c1", 1000);
    Uplata *u2 = new Uplata("a2", "b2", "c2", 100);
    Uplata *u3 = new Uplata("c3", "b3", "c3", 1400);
    Isplata *i1 = new Isplata("x1", "y1", "z1", 250);
    Isplata *i2 = new Isplata("x2", "y2", "z2", 250);
    Isplata *i3 = new Isplata("x3", "y3", "z3", 1000);
    Racun r(1000);
    r.izvrsiUplatu(u1);
    r.izvrsiUplatu(u2);
    r.izvrsiUplatu(u3);
    r.izvrsiIsplatu(i1);
    r.izvrsiIsplatu(i2);
    r.izvrsiIsplatu(i3);
    r.izvestaj();
    return 0;
}