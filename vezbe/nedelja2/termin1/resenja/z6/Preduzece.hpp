#ifndef PREDUZECE_HPP
#define PREDUZECE_HPP

#include <iostream>
#include <string>
#include <vector>

#include "Radnik.hpp"

class Preduzece
{
private:
    std::string naziv;
    std::string adresa;
    std::vector<Radnik> radnici;

public:
    Preduzece();
    Preduzece(std::string naziv, std::string adresa);
    std::string getNaziv();
    void setNaziv(std::string naziv);
    std::string getAdresa();
    void setAdresa(std::string adresa);
    int getBrojRadnika();
    void zaposliRadnika(Radnik &radnik);
    double getMesecniTroskovi();
    void spisakZaposlenih();
};

#endif