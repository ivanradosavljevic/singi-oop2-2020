#ifndef SASTOJAK_HPP
#define SASTOJAK_HPP

#include <iostream>
#include <string>

using namespace std;

class Sastojak
{
private:
    string naziv;
    double kolicina;

public:
    Sastojak();
    Sastojak(string naziv, double kolicina);
    string getNaziv();
    void setNaziv(string naziv);
    double getKolicina();
    void setKolicina(double kolicina);
    void detalji();
};

#endif