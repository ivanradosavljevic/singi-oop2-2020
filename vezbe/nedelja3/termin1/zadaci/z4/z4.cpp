/**
 * Prepraviti prethodni zadatak tako da se
 * omogući dodavanje proizvoljnog tipa osobe.
 * Neki od tipova osobe su radnik, vozač koji
 * proširuje radnika i komercijalista koji
 * proširuje radnika.
 * Prepraviti widget za prikaz radnika tako da
 * se radnici prikazuju kao lista koja u sebi
 * prikazuje sve podatke o radniku.
 */