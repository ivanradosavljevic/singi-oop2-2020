#include "BudzetPrikaz.hpp"

BudzetPrikaz::BudzetPrikaz(int x, int y, int w, int h,
                           double budzet, double troskovi) : Fl_Group(x, y, w, h),
                                                             budzet(budzet),
                                                             troskovi(troskovi)
{
    budzetBox = new Fl_Box(x, y, w, h);
    budzetBox->color(fl_rgb_color(0, 255, 0));
    budzetBox->box(FL_BORDER_BOX);
    budzetBox->align(FL_ALIGN_RIGHT | FL_ALIGN_INSIDE);
    troskoviBox = new Fl_Box(x, y, 0, h);
    troskoviBox->color(fl_rgb_color(255, 0, 0));
    troskoviBox->box(FL_BORDER_BOX);
    troskoviBox->align(FL_ALIGN_RIGHT | FL_ALIGN_INSIDE);
    azurirajPrikaz();
    this->end();
}
void BudzetPrikaz::azurirajPrikaz()
{
    stringstream sstream;
    sstream << budzet;
    budzetBox->resize(x(), y(), w(), h());
    budzetBox->copy_label(sstream.str().c_str());
    sstream.str("");
    sstream << troskovi;
    troskoviBox->resize(x(), y(), w() * (troskovi / budzet), h());
    troskoviBox->copy_label(sstream.str().c_str());
    sstream.str("");
    sstream << troskovi << "/" << budzet;
    copy_label(sstream.str().c_str());
}
double BudzetPrikaz::getBudzet()
{
    return budzet;
}
void BudzetPrikaz::setBudzet(double budzet)
{
    this->budzet = budzet;
    azurirajPrikaz();
}
double BudzetPrikaz::getTroskovi()
{
    return troskovi;
}
void BudzetPrikaz::setTroskovi(double troskovi)
{
    this->troskovi = troskovi;
    azurirajPrikaz();
}
BudzetPrikaz::~BudzetPrikaz()
{
}