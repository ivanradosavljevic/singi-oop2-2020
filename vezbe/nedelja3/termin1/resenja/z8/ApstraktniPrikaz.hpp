#ifndef APSTRAKTNI_PRIKAZ_HPP
#define APSTRAKTNI_PRIKAZ_HPP

template <typename T>
class ApstraktniPrikaz
{
public:
    virtual void elementUmetnut(int indeks, T element) = 0;
    virtual void elementUklonjen(int indeks) = 0;
    virtual ~ApstraktniPrikaz(){};
};

#endif