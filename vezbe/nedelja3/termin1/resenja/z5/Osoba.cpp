#include "Osoba.hpp"

Osoba::Osoba() {}
Osoba::Osoba(string ime, string prezime) : ime(ime), prezime(prezime) {}

string Osoba::getIme()
{
    return ime;
}

void Osoba::setIme(string ime)
{
    this->ime = ime;
}
string Osoba::getPrezime()
{
    return prezime;
}
void Osoba::setPrezime(string prezime)
{
    this->prezime = prezime;
}

string Osoba::getTip()
{
    return "osoba";
}
void Osoba::zapisi(ostream &output)
{
    output << getTip() << " " << ime << endl << prezime << endl;
}
void Osoba::procitaj(istream &input)
{
    input >> ws;
    getline(input, ime, '\n');
    getline(input, prezime, '\n');
}

Osoba::~Osoba()
{
}