/**
 * Prepraviti prethodni zadatak dodavanjem
 * widgeta za prikaz troškova i budžeta firme.
 * Ovaj widget ispisuje troškove i budžet
 * razdovjene simbolom /, npr.: 20000/12000.
 * gde prvi broj predstavlja troškove a drugi budžet.
 * Ispod ovog ispisa nacrtati pravougaonik čija dužina
 * je prorporcionalna visini budžeta i čija boja je zelena.
 * Preko ovog pravougaonika iscrtati crveni pravougaonik
 * čija je dužina proporcionalna troškovima. Na krajevima
 * ovih prvaougaonika ispisati troškove odnosno budžet.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "BudzetPrikaz.hpp"

#include "Osoba.hpp"
#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Komercijalista.hpp"

using namespace std;

ostream &operator<<(ostream &output, Osoba *osoba)
{
    osoba->zapisi(output);
    return output;
}

istream &operator>>(istream &input, Osoba *osoba)
{
    osoba->procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, vector<Osoba *> &osobe)
{
    output << osobe.size() << endl;
    for (int i = 0; i < osobe.size(); i++)
    {
        output << osobe.at(i) << endl;
    }
    return output;
}

istream &operator>>(istream &input, vector<Osoba *> &osobe)
{
    int brojOsoba;
    input >> brojOsoba;
    string tip;
    for (int i = 0; i < brojOsoba; i++)
    {
        Osoba *novaOsoba;
        input >> tip;
        if (tip == "osoba")
        {
            novaOsoba = new Osoba();
        }
        else if (tip == "radnik")
        {
            novaOsoba = new Radnik();
        }
        else if (tip == "vozac")
        {
            novaOsoba = new Vozac();
        }
        else if (tip == "komercijalista")
        {
            novaOsoba = new Komercijalista();
        }
        input >> novaOsoba;
        osobe.push_back(novaOsoba);
    }
    return input;
}

int main()
{
    vector<Osoba *> osobe;

    ifstream file("osobe.txt");
    file >> osobe;
    file.close();

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 5");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 240, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 150, &osobe);
    BudzetPrikaz *budzetPrikaz = new BudzetPrikaz(400, 180, 300, 30, 120000, 40000);

    window->end();
    window->show();
    return Fl::run();
}