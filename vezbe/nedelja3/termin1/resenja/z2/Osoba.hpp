#ifndef OSOBA_HPP
#define OSOBA_HPP

#include <iostream>
#include <string>

using namespace std;

class Osoba
{
private:
    string ime;
    string prezime;

public:
    Osoba();
    Osoba(string ime, string prezime);
    string getIme();
    void setIme(string ime);
    string getPrezime();
    void setPrezime(string prezime);
    virtual ~Osoba();
};

#endif