#ifndef PRIKAZ_OSOBA_HPP
#define PRIKAZ_OSOBA_HPP

#include <vector>
#include <sstream>

#include <FL/Fl_Group.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>

#include "Osoba.hpp"

using namespace std;

class PrikazOsoba : public Fl_Group
{
protected:
    Fl_Output *imeOutput;
    Fl_Output *prezimeOutput;
    Fl_Button *sledeciButton;
    Fl_Button *prethodniButton;

    int trenutni = -1;
    vector<Osoba> *osobe;

    void azurirajLabelu();
    void proveraDugmica();
    void postaviPrikaz(int indeks);

public:
    PrikazOsoba(int x, int y, int w, int h, vector<Osoba> *osobe);
    virtual ~PrikazOsoba();

    static void predjiNaSledeci(Fl_Widget *widget, void *data);
    static void predjiNaPrethodni(Fl_Widget *widget, void *data);
};

#endif