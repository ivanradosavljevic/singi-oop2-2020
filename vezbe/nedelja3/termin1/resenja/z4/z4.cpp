/**
 * Prepraviti prethodni zadatak tako da se
 * omogući dodavanje proizvoljnog tipa osobe.
 * Neki od tipova osobe su radnik, vozač koji
 * proširuje radnika i komercijalista koji
 * proširuje radnika.
 * Prepraviti widget za prikaz radnika tako da
 * se radnici prikazuju kao lista koja u sebi
 * prikazuje sve podatke o radniku.
 */

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "FormaOsoba.hpp"
#include "PrikazOsoba.hpp"
#include "Osoba.hpp"
#include "Radnik.hpp"
#include "Vozac.hpp"
#include "Komercijalista.hpp"

using namespace std;

ostream &operator<<(ostream &output, Osoba *osoba)
{
    osoba->zapisi(output);
    return output;
}

istream &operator>>(istream &input, Osoba *osoba)
{
    osoba->procitaj(input);
    return input;
}

ostream &operator<<(ostream &output, vector<Osoba *> &osobe)
{
    output << osobe.size() << endl;
    for (int i = 0; i < osobe.size(); i++)
    {
        output << osobe.at(i) << endl;
    }
    return output;
}

istream &operator>>(istream &input, vector<Osoba *> &osobe)
{
    int brojOsoba;
    input >> brojOsoba;
    string tip;
    for (int i = 0; i < brojOsoba; i++)
    {
        Osoba *novaOsoba;
        input >> tip;
        if (tip == "osoba")
        {
            novaOsoba = new Osoba();
        }
        else if (tip == "radnik")
        {
            novaOsoba = new Radnik();
        }
        else if (tip == "vozac")
        {
            novaOsoba = new Vozac();
        }
        else if (tip == "komercijalista")
        {
            novaOsoba = new Komercijalista();
        }
        input >> novaOsoba;
        osobe.push_back(novaOsoba);
    }
    return input;
}

int main()
{
    vector<Osoba *> osobe;

    ifstream file("osobe.txt");
    file >> osobe;
    file.close();

    Fl_Window *window = new Fl_Window(800, 600, "Zadatak 4");
    FormaOsoba *formaOsoba = new FormaOsoba(0, 50, 300, 240, "Podaci o osobi", &osobe);
    PrikazOsoba *prikazOsoba = new PrikazOsoba(400, 50, 300, 150, &osobe);

    window->end();
    window->show();
    return Fl::run();
}