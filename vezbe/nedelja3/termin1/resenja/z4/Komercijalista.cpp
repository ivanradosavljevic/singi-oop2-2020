#include "Komercijalista.hpp"

Komercijalista::Komercijalista() : Radnik()
{
}
Komercijalista::Komercijalista(string ime, string prezime,
                               double visinaPlate, string odeljenje) : Radnik(ime, prezime, visinaPlate),
                                                                       odeljenje(odeljenje)
{
}
string Komercijalista::getOdeljenje()
{
    return odeljenje;
}
void Komercijalista::setOdeljenje(string odeljenje)
{
    this->odeljenje = odeljenje;
}
string Komercijalista::getTip()
{
    return "komercijalista";
}
void Komercijalista::zapisi(ostream &output)
{
    Radnik::zapisi(output);
    output << " " << odeljenje << endl;
}
void Komercijalista::procitaj(istream &input)
{
    Radnik::procitaj(input);
    getline(input, odeljenje, '\n');
}
Komercijalista::~Komercijalista()
{
}