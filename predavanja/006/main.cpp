#include <vector>
#include <string>
#include <sstream>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Table_Row.H>
#include <FL/Fl_Spinner.H>


using namespace std;

class Radnik
{
private:
  string ime;
  string prezime;
  double plata;

public:
  Radnik(string ime, string prezime, double plata) : ime(ime), prezime(prezime), plata(plata) {}

  string getIme() { return ime; }
  string getPrezime() { return prezime; }
  double getPlata() { return plata; }

  void setIme(string ime) { this->ime = ime; }
  void setPrezime(string prezime) { this->prezime = prezime; }
  void setPlata(double plata) { this->plata = plata; }
};

class TabelaRadnika : public Fl_Table_Row
{
protected:
  vector<Radnik *> radnici;
  void draw_cell(TableContext context,
                 int row = 0, int column = 0, int x = 0, int y = 0, int w = 0, int h = 0);

public:
  TabelaRadnika(int x, int y, int w, int h, const char *l = 0) : Fl_Table_Row(x, y, w, h, l)
  {
    end();
  }
  void add(Radnik *r)
  {
    this->radnici.push_back(r);
    this->rows(radnici.size());
  }
  ~TabelaRadnika() {}
};


void TabelaRadnika::draw_cell(TableContext context,
                              int row, int column, int x, int y, int w, int h)
{
  switch (context)
  {
  case CONTEXT_STARTPAGE:
    fl_font(FL_HELVETICA, 16);
    return;

  case CONTEXT_COL_HEADER:
    fl_push_clip(x, y, w, h);
    {
      fl_draw_box(FL_THIN_UP_BOX, x, y, w, h, col_header_color());
      fl_color(FL_BLACK);
      if (column == 0)
      {
        fl_draw("Ime", x, y, w, h, FL_ALIGN_CENTER);
      }
      else if (column == 1)
      {
        fl_draw("Prezime", x, y, w, h, FL_ALIGN_CENTER);
      }
      else if (column == 2)
      {
        fl_draw("Plata", x, y, w, h, FL_ALIGN_CENTER);
      }
    }
    fl_pop_clip();
    return;

  case CONTEXT_CELL:
  {
    fl_push_clip(x, y, w, h);
    {
      fl_color(row_selected(row) ? FL_SELECTION_COLOR : FL_WHITE);
      fl_rectf(x, y, w, h);
      fl_color(FL_BLACK);
      if (column == 0)
      {
        fl_draw(radnici[row]->getIme().c_str(), x, y, w, h, FL_ALIGN_LEFT);
      }
      else if (column == 1)
      {
        fl_draw(radnici[row]->getPrezime().c_str(), x, y, w, h, FL_ALIGN_LEFT);
      }
      else if (column == 2)
      {
        ostringstream out;
        out.precision(2);
        out << fixed << radnici[row]->getPlata();
        fl_draw(out.str().c_str(), x, y, w, h, FL_ALIGN_RIGHT);
      }

      fl_color(color());
      fl_rect(x, y, w, h);
    }
    fl_pop_clip();
    return;
  }

  case CONTEXT_ROW_HEADER:
  case CONTEXT_TABLE:
  case CONTEXT_ENDPAGE:
  case CONTEXT_RC_RESIZE:
  case CONTEXT_NONE:
    return;
  }
}

int main(int argc, char **argv){
    Fl_Window win(800, 315, "Neki naslov na prozoru");

    TabelaRadnika *tabela = new TabelaRadnika(20, 20, 760, 250, "Tabela radnika");

    tabela->when(FL_WHEN_RELEASE | FL_WHEN_CHANGED);
    tabela->table_box(FL_NO_BOX);
    tabela->col_resize_min(4);
    tabela->row_resize_min(4);
    tabela->row_height_all(20);
    tabela->col_resize(1);
    tabela->col_header(1);
    tabela->col_header_height(25);
    tabela->col_width_all(80);
    tabela->cols(3);

    tabela->add(new Radnik("Marko", "Petrović", 75000));
    tabela->add(new Radnik("Jovan", "Grujić", 56700));
    tabela->add(new Radnik("Marina", "Jakovljević", 145000));
    tabela->add(new Radnik("Jelena", "Mitrović", 88600));

    win.begin();

  Fl_Input *imeInput = new Fl_Input(50, 280, 120, 25, "Ime:");
  Fl_Input *prezimeInput = new Fl_Input(235, 280, 120, 25, "Prezime:");
  Fl_Spinner *plataInput = new Fl_Spinner(405, 280, 120, 25, "Plata:");
  plataInput->minimum(0);
  plataInput->maximum(1000000);
  Fl_Button *dodajDugme = new Fl_Button(540, 280, 120, 25, "Dodaj radnika");


  vector<Fl_Widget*> *widgets = new vector<Fl_Widget*>();
  widgets->push_back(tabela);
  widgets->push_back(imeInput);
  widgets->push_back(prezimeInput);
  widgets->push_back(plataInput);


  dodajDugme->callback([](Fl_Widget *w, void *u){
    vector<Fl_Widget*> *widgets = (vector<Fl_Widget*>*)u;
    TabelaRadnika *tabela = (TabelaRadnika*)widgets->at(0);
    Fl_Input *imeInput = (Fl_Input*)widgets->at(1);
    Fl_Input *prezimeInput = (Fl_Input*)widgets->at(2);
    Fl_Spinner *plataInput = (Fl_Spinner*)widgets->at(3);

    if(string(imeInput->value()) == "" || string(prezimeInput->value()) == "") {
      return;
    }

    tabela->add(new Radnik(imeInput->value(), prezimeInput->value(), plataInput->value()));
  }, widgets);

  win.end();
  win.resizable(*tabela);
  win.show(argc, argv);

  return (Fl::run());



}