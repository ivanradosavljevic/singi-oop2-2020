#ifndef GLAVNI_PROZOR_HPP
#define GLAVNI_PROZOR_HPP

#include <FL/Fl.H>
#include <Fl/Fl_Widget.H>
#include <FL/Fl_Window.H>

#include <string>

using namespace std;

class GlavniProzor : public Fl_Window
{
protected:
    Fl_Group *layout;

public:
    GlavniProzor(const string &title, Fl_Group *layout = nullptr);
    void dodajKomponentu(Fl_Widget *komponenta);
    void setLayout(Fl_Group *layout);
    int run();
};

#endif