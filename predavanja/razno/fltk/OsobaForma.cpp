#include "OsobaForma.hpp"

OsobaForma::OsobaForma(vector<Osoba*>&osobe) : Fl_Pack(0, 0, 200, 200), osobe(osobe)
{
    this->type(Fl_Pack::VERTICAL);
    this->spacing(2);
    this->imeInput = new Fl_Input(0, 0, 200, 30, "Ime:");
    this->prezimeInput = new Fl_Input(0, 0, 200, 30, "Prezime:");
    this->dodajButton = new Fl_Button(0, 0, 200, 30, "Dodaj osobu");
    this->add(this->imeInput);
    this->add(this->prezimeInput);
    this->add(dodajButton);
    this->dodajButton->callback(onDodaj, this);
}

void OsobaForma::onDodaj(Fl_Widget* widget, void* data)
{
    OsobaForma *forma = (OsobaForma*)data;
    forma->osobe.push_back(new Osoba(forma->imeInput->value(), forma->prezimeInput->value()));
}