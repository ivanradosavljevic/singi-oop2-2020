#include "Osoba.hpp"

Osoba::Osoba()
{
}
Osoba::Osoba(const string &ime, const string &prezime) : ime(ime), prezime(prezime)
{
}
string Osoba::getIme() const
{
    return ime;
}
void Osoba::setIme(const string &ime)
{
    this->ime = ime;
}
string Osoba::getPrezime() const
{
    return prezime;
}
void Osoba::setPrezime(const string &prezime)
{
    this->prezime = prezime;
}